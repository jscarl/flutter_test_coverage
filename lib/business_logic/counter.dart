class Counter {
  int _counter = 0;
  int get value => _counter;

  void increment() => _counter++;
  void decrement() => _counter--;
}
